#!/bin/bash

# python version
defaul_ver=python3.7
# name of virtual environment directory
venv=venv
# set to true if you need latest version
latest_ver=true

# find latest version of python
if [ latest_ver ]
then
  ver=$(find /usr/bin/ -regextype awk  -regex ".*\/python([0-9]+(\.[0-9]+)*)?" | sort | tail -1)
fi

if [ $ver == "" ]
then
  echo "Don't find any python interpreter. Using default: $defaul_ver"
  python=$defaul_ver
else
  echo "Find latest python version: $ver"
  python=$ver
fi


echo "Creating virtual environment: \"$(pwd)/$venv\""
# create venv
$python -m venv --copies $venv
# go to venv and activate one
cd ./$venv/bin/
source activate
# upgrade pip itself
echo "Upgrading pip:"
pip install -U pip
# install packages
echo "Installing packages:"
pip install -r ../../requirements.txt
# deactivate venv
deactivate
