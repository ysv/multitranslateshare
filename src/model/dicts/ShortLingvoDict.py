# -*- coding: utf-8 -*-
from model.dicts.BaseDict import BaseDict
from exceptions import Errors

class ShortLingvoDict(BaseDict):
    
    # Languages and their codes             
    LANGS = {
        'eng': '1033',                         # English
        'rus': '1049'                          # Russian
#         'zho': '1028',
        
    }
    
    # Authorization api key
    __KEY__: str = 'OTA1YWNiZjctOTY1Ni00NzBjLWEyMTktMmU1OWNhOTdlNTBiOjI1ZjVlMTU5MDJjMzRmYTBhNTA0ZGIyZTU4YzZjYmY5'
    
    # Authorization uri
    __AUTH_URI__: str = 'https://developers.lingvolive.com/api/v1.1/authenticate'
    
    # Short translation uri
    __TRAN_URI__: str = 'https://developers.lingvolive.com/api/v1/Minicard'
    
    def __init__(self, src_lang: str, dst_lang: str, **kwds):
        if src_lang not in self.LANGS:
            raise Errors.TranslationError(f'mt: LingvoDict: not supported source language \'{src_lang}\'.')
        if dst_lang not in self.LANGS:
            raise Errors.TranslationError(f'mt: LingvoDict: not supported source language \'{dst_lang}\'.')
        self.__src_lang__: str = self.LANGS[src_lang]         # code of source language    
        self.__dst_lang__: str = self.LANGS[dst_lang]         # code of destination language
        self.__src_langStr__ = src_lang                                 # text repr of source language
        self.__dst_langStr__ = dst_lang                                 # text repr of destination language
        super().__init__(**kwds)
           
    def translate(self, adapter) -> BaseDict.Translation:
        """Translate content from adapter"""
        for txt in adapter:
            # translate text
            tran = self.__translate__(txt)
            # return translation 
            self.receiver.set_translation(tran)
            
    def __translate__(self, text: str):
        """Translate the text."""
        bearer = self.__getBearer__()        
        # build uri for translation
        payload = {
            'text': text,
            'srcLang': self.__src_lang__,
            'dstLang': self.__dst_lang__
        }
        try:
            response = self.__get__(uri=self.__TRAN_URI__, headers={'Authorization': f'Bearer {bearer}'}
                                    , params=payload)
        except Errors.HTTPRequestError as e:
            if e.status_code == 404:
                raise Errors.TranslationNotFoundError(
                    message=f'mt: ABBYY Lingvo Live: Translation for "{text}" hasn\'t found')
            else:
                raise
        else:
            # parse response and return translation 
            res = self.__parseShort__(response.json())
            return self.Translation(source=text, tran=res
                                    , src_lang=self.__src_langStr__, dst_lang=self.__dst_langStr__)

    def __getBearer__(self) -> str:
        """Sends request for getting authorization token."""
        try:        
            r = self.__post__(uri=self.__AUTH_URI__
                                , headers={'Authorization': f'Basic {self.__KEY__}'})
        except Errors.RequestError as e:
            print(e.message)
            return None
        else:        
            return r.text
        
    def __parseShort__(self, dct: dict) -> str:
        """Parse json response from server for short translation"""
        if 'Translation' not in dct:
            raise Errors.ParseJsonError(
                message='mt: Internal error: ABBYY Lingvo Live: Wrong json format of answer.')
        tran = dct['Translation']
        return tran['Translation']
