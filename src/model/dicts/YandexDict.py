# -*- coding: utf-8 -*-
from model.dicts.BaseDict import BaseDict
from exceptions import Errors

class YandexDict(BaseDict):    
    
    # Languages
    LANGS = {
        'aze': 'az',       # Azerbaijan 
        'sqi': 'sq',       # Albanian
        'amh': 'am',       # Amharic
        'eng': 'en',       # English
        'ara': 'ar',       # Arab
        'hye': 'hy',       # Armenian
        'afr': 'af',       # Afrikaans
        'eus': 'eu',       # Basque
        'bak': 'ba',       # Bashkir
        'bel': 'be',       # Belorussian
        'ben': 'bn',       # Bengal
        'mya': 'my',       # Burmese
        'bul': 'bg',       # Bulgarian
        'bos': 'bs',       # Bosnian
        'cym': 'cy',       # Welsh        
        'hun': 'hu',       # Hungarian
        'vie': 'vi',       # Vietnamese
        'htn': 'ht',       # Haitian (Creole)
        'glg': 'gl',       # Galician
        'nld': 'nl',       # Dutch
        'mrj': 'mrj',      # Gorno-Mari
        'ell': 'el',       # Greek
        'kat': 'ka',       # Georgian
        'guj': 'gu',       # Gujarati
        'dan': 'da',       # Danish
        'heb': 'he',       # Hebrew
        'yid': 'yi',       # Yiddish
        'ind': 'id',       # Indonesian
        'gle': 'ga',       # Irish
        'ita': 'it',       # Italian
        'isl': 'is',       # Icelandic
        'spa': 'es',       # Spanish
        'kaz': 'kk',       # Kazakh
        'kan': 'kn',       # Kannada
        'cat': 'ca',       # Catalan
        'kir': 'ky',       # Kyrgyz
        'zho': 'zh',       # Chinese
        'kor': 'ko',       # Korean
        'xho': 'xh',       # Xhosa
        'khm': 'km',       # Central Khmer
        'lao': 'lo',       # Laotian
        'lat': 'la',       # Latin
        'lav': 'lv',       # Latvian
        'lit': 'lt',       # Lithuanian
        'ltz': 'lb',       # Luxembourgish
        'mlg': 'mg',       # Malagasy 
        'msa': 'ms',       # Malay 
        'mal': 'ml',       # Malayalam 
        'mlt': 'mt',       # Maltese 
        'mkd': 'mk',       # Macedonian 
        'mri': 'mi',       # Maori 
        'mar': 'mr',       # Marathi
        'mhr': 'mhr',      # Mari
        'mon': 'mn',       # Mongolian 
        'deu': 'de',       # Deutsch 
        'nep': 'ne',       # Nepali
        'nor': 'no',       # Norwegian 
        'pan': 'pa',       # Punjabi 
        'pap': 'pap',      # Papiamento 
        'fas': 'fa',       # Persian 
        'pol': 'pl',       # Polish
        'por': 'pt',       # Portuguese 
        'ron': 'ro',       # Romanian 
        'rus': 'ru',       # Russian  
        'ceb': 'ceb',      # Cebuan 
        'srp': 'sr',       # Serbian 
        'sin': 'si',       # Sinhalese 
        'slk': 'sk',       # Slovak
        'slv': 'sl',       # Slovenian
        'swa': 'sw',       # Swahili 
        'sun': 'su',       # Sundanese 
        'tgk': 'tg',       # Tajik 
        'tha': 'th',       # Thai 
        'tgl': 'tl',       # Tagalog 
        'tam': 'ta',       # Tamil 
        'tat': 'tt',       # Tatar               
        'tel': 'te',       # Telugu 
        'tur': 'tr',       # Turkish 
        'udm': 'udm',      # Udmurt 
        'uzb': 'uz',       # Uzbek 
        'ukr': 'uk',       # Ukrainian
        'urd': 'ur',       # urdu 
        'fin': 'fi',       # Finnish 
        'fra': 'fr',       # French 
        'hin': 'hi',       # Hindi
        'hrv': 'hr',       # Croatian               
        'ces': 'cs',       # Czech 
        'swe': 'sv',       # Swedish 
        'gla': 'gd',       # Scottish 
        'est': 'et',       # Estonian 
        'epo': 'eo',       # Esperanto
        'jav': 'jv',       # Javanese 
        'jpn': 'ja',       # Japanese 
    }
    
    # Authorization api key
    __KEY__ = 'trnsl.1.1.20190219T080827Z.9e37482276542aed.c700dd2dcb53b8bcac5f97738cd8328264cac448'
    
    # Translation uri
    __TRAN__ = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
    
    def __init__(self, src_lang: str, dst_lang: str, **kwds):
        if src_lang not in self.LANGS:
            raise Errors.TranslationError(f'mt: YandexDict: not supported source language \'{src_lang}\'.')
        if dst_lang not in self.LANGS:
            raise Errors.TranslationError(f'mt: YandexDict: not supported source language \'{dst_lang}\'.')
        self.__src_lang__ = self.LANGS[src_lang]                  # source language 
        self.__dst_lang__ = self.LANGS[dst_lang]                  # destination language
        super().__init__(**kwds)
        
    def translate(self, adapter) -> BaseDict.Translation:
        """Translate content from adapter"""
        # Divide text into parts for post request 
        for text in adapter:
            tran = self.__translate__(text)
            self.receiver.set_translation(tran)
                
    def __translate__(self, text):
        """Translate the text."""
        payload = {
            'key': self.__KEY__,
            'lang': f'{self.__src_lang__}-{self.__dst_lang__}'
        }
        try:
            response = self.__post__(uri = self.__TRAN__, params=payload, data={'text': text})
        except Errors.HTTPRequestError as e:
            if e.status_code == 404:
                raise Errors.TranslationError(
                    message='mt: Yandex Translate: The daily translation limit is exhausted')
            elif e.status_code == 422:
                raise Errors.TranslationNotFoundError(
                    message=f'mt: Yandex Translate: Translation for "{text}" hasn\'t found')
            elif e.status_code == 501:
                raise Errors.TranslationError(
                    message='mt: Yandex Translate: Destination language isn\'t supported')
            else:
                raise
        else:
            res = self.__parse__(response.json())
            return self.Translation(source=text, tran=res
                                    , src_lang=self.__src_lang__, dst_lang=self.__dst_lang__)
    def __parse__(self, dct: dict) -> str:
        """Parse json response from server."""
        if 'text' not in dct:
            raise Errors.ParseJsonError(
                message='mt: Internal error: Yandex Translate: Wrong json format of answer.')
        else:
#             TODO: remove patch
            return dct['text'][0]
        