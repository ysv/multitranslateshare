# -*- coding: utf-8 -*-
from model.dicts.BaseDict import BaseDict
from model.dicts.ShortLingvoDict import ShortLingvoDict
from exceptions import Errors

class FullLingvoDict(ShortLingvoDict):
    
    # Full translation uri
    __TRAN_URI__: str = 'https://developers.lingvolive.com/api/v1/Article'
    
    # Nesting depth
    __INDENT_COUNT__:int = -1
    
    # Indentation "char"
    __INDENT__: str = '    '
    
    # Languages abbreviation
    __LANGS_ABB__ = {
        'eng': 'En',                         # English
        'rus': 'Ru'                          # Russian
    }
    
    def __init__(self, src_lang: str, dst_lang: str, **kwds):
        self.__src_abb__ = self.__LANGS_ABB__[src_lang]
        self.__dst_abb__ = self.__LANGS_ABB__[dst_lang]
        super().__init__(src_lang, dst_lang, **kwds)
    
    def translate(self, adapter) -> BaseDict.Translation:
        """Translate content from adapter"""
        for txt in adapter:
            # translate text
            tran = self.__translate__(txt)
            # return translation 
            self.receiver.set_translation(tran)
            
    def __translate__(self, text:str):
        # TODO: Don't repeat yourself
        """Translate the text."""
        bearer = self.__getBearer__()        
        # build uri for translation
        payload = {
            'heading': text,
            'dict': f'LingvoUniversal ({self.__src_abb__}-{self.__dst_abb__})',
            'srcLang': self.__src_lang__,
            'dstLang': self.__dst_lang__
        }
        try:
            response = self.__get__(uri=self.__TRAN_URI__, headers={'Authorization': f'Bearer {bearer}'}
                                    , params=payload)
        except Errors.HTTPRequestError as e:
            if e.status_code == 404:
                raise Errors.TranslationNotFoundError(
                    message=f'mt: ABBYY Lingvo Live: Translation for "{text}" hasn\'t found')
            else:
                raise
        else:
            # parse response and return translation 
            res = self.__parse_full__(response.json())
            return self.Translation(source=text, tran=res
                                    , src_lang=self.__src_langStr__, dst_lang=self.__dst_langStr__)

    def __parse_full__(self, dct: dict) -> str:
        """Parse json response from server for full translation.
        Response is set of article nodes. It has a tree structure. Any article node can have
        a lot of ones into itself. Final nodes in each branch are nodes of type 'text'.
        Content of those ones is translation. In common cases 'text' article nodes are included
        into nodes of type 'paragraph', 'comment', 'transcription'. There are article nodes
        of type 'list'. Items of list have indentation, which is count of lists before the current
        one into tree hierarchy."""
        txt = ''
        if 'Body' not in dct:
            raise Errors.ParseJsonError(
                message='mt: Internal error: ABBYY Lingvo Live: Wrong json format of answer.')
        for node in dct['Body']:
            txt += self.__parse_node__(node)
        return f'\n{dct["Title"]}\n{txt}'
            
    def __parse_node__(self, node) -> str:
        """Parse articleNode. It can be of variouse type."""
        if 'Node' not in node:
            raise Errors.ParseJsonError(
                message='mt: Internal error: ABBYY Lingvo Live: Wrong json format of answer.')
        nodeType = node['Node']
        if nodeType == 'List':
            return self.__parse_list__(node)
        elif nodeType == 'ListItem':
            return self.__parse_list_item__(node)
        elif nodeType == 'Paragraph':
            return self.__parse_paragraph__(node)
        elif nodeType == 'Comment':
            return self.__parse_comment__(node)
        elif nodeType == 'Transcription':
            return self.__parse_transcript__(node)
        elif nodeType == 'Text' or nodeType == 'Abbrev':
            return self.__parse_text__(node)
        else:
            return ''
            
    def __parse_list__(self, node) -> str:
        """Parse articleNode of type 'list'."""
        txt = ''
        if 'Items' not in node: 
            raise Errors.ParseJsonError(
                    message='mt: Internal error: ABBYY Lingvo Live: Wrong json format of answer.')
        self.__INDENT_COUNT__ += 1
        for n, item in enumerate(node['Items']):
            num = self.__get_number__(self.__INDENT_COUNT__)
            txt += f'\n{self.__INDENT__*self.__INDENT_COUNT__}{num+str(n+1)}) {self.__parse_node__(item)}'
        self.__INDENT_COUNT__ -= 1
        return txt         
    
    def __get_number__(self, num: int) -> str:
        """Convert decimal number to it's str representation.
        e.g. 3 -> 1.2.3."""
        txt = ''
        for i in range(num+1):
            if i == 0: continue
            txt += str(i) + '.'
        return txt
    
    def __parse_list_item__(self, node) -> str:
        """Parse articleNode of type 'list item'."""
        return self.__parse_stand_node__(node)
    
    def __parse_comment__(self, node) -> str:
        """Parse articleNode of type 'comment'."""
        return self.__parse_stand_node__(node)

    def __parse_paragraph__(self, node) -> str:
        """Parse articleNode of type 'paragraph'."""
        return self.__parse_stand_node__(node)
    
    def __parse_stand_node__(self, node) -> str:
        """Parse common articleNode with field 'Markup'."""
        txt = ''
        if 'Markup' not in node:
            raise Errors.ParseJsonError(
                    message='mt: Internal error: ABBYY Lingvo Live: Wrong json format of answer.')
        for item in node['Markup']:
            txt += self.__parse_node__(item)
        return txt

    def __parse_transcript__(self, node) -> str:
        """Parse articleNode of type 'transcription'."""
        return '['+self.__parse_text__(node)+']'
    
    def __parse_text__(self, node) -> str:
        """Parse articleNode of type 'text'."""
        if 'Text' not in node:
            raise Errors.ParseJsonError(
                    message='mt: Internal error: ABBYY Lingvo Live: Wrong json format of answer.')
        return node['Text']
    