# -*- coding: utf-8 -*-
from model.dicts.ShortLingvoDict import ShortLingvoDict
from model.dicts.YandexDict import YandexDict

# Dictionaries
DICTS = [
        'ya',                       # yandex translate
        'ling'                      # abbyy lingvo
    ]

# Available languages for translation
LANGS = set(list(ShortLingvoDict.LANGS)) | set(list(YandexDict.LANGS)) 


class Separators:
    """Separators to split input text per words or expressions.
    See TextAdapters.Spliter""" 
    WORD = 'w',    # words
    EXPRESSION = 'ex'    # expression
