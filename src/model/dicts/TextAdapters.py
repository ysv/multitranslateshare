# -*- coding: utf-8 -*-
import re


class PartIter:
    """Iterates over data per part. Data must be iterable."""
    
    def __init__(self, data, part: int=1): 
        self.data = data            # data
        self.part = part            # size of part
        self.start = 0              # start index of part 
        self.end = part             # end index of part
    
    def __iter__(self):
        return self
            
    def __next__(self):
        if self.start > self.data.__len__()-1:
            raise StopIteration
        part = self.data[self.start:self.end]
        self.start, self.end = self.end, self.end+self.part
        return part
    

class Spliter:
    """Split text into separate words (by default) or separate expressions.
    Words are defined via whitespaces, any non-word characters are ignored.
    Each expression must begin and end with /*/ ."""
    
    # Pattern to find words divided by whitespaces. Any non-word characters are ignored.    
    WHITE_SPACES = r"\s*\W*(\w*)\W*\s*"
    
    # Pattern to find expressions divided by /*/ characters.
    EXPRESSION = r"/\*/(\w*\s*\w*)/\*/"
    
    def __init__(self, text, sep: str=WHITE_SPACES):
        self.text = text            # text
        self.patt = sep             # pattern
        
    def __iter__(self):
        def gen():
            for match in re.finditer(self.patt, self.text):
                if match:
                    yield match[1]
        return gen() 
    
        
        
