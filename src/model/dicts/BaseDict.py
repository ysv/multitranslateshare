# -*- coding: utf-8 -*-
import requests
import abc
from exceptions.Errors import RequestError, HTTPRequestError

class Receiver(metaclass=abc.ABCMeta):
    """Interface to receive translation. It is needed to proceed each translation 
    one at time and don't take up memory."""
    
    @abc.abstractmethod
    def set_translation(self, tran):
        """Send translation from dictionary."""
        pass
    
    
class BaseDict(metaclass=abc.ABCMeta):
    """Abbreviation of language are as in ISO 639-3."""
    
    class Translation:
        """Translation from server after parsing"""
        def __init__(self, source: str=None, tran: str=None, src_lang: str=None, dst_lang: str=None):
            self.source = source                              # source word
            self.tran = tran                                  # translation word
            self.src_lang = src_lang                          # source language
            self.dst_lang = dst_lang                          # destination language
        
    @abc.abstractmethod
    def __init__(self, receiver: Receiver):
        self.receiver = receiver
        
    @abc.abstractmethod    
    def translate(self, txtAdapter):
        """Translate text from textAdapter. textAdapter must be iterable."""
        pass

    def __request__(self, meth: str, uri: str, timeout: float=10.0, **kwds) -> requests.Response:
        """Perform http request to uri by method meth."""
        try:
            r = requests.request(meth, uri, timeout=timeout, **kwds)
            r.raise_for_status()
            return r
        except requests.exceptions.Timeout:
            raise RequestError(message='Request to ' + uri + ' timed out. Timeout=' + timeout)
        except requests.exceptions.ConnectionError:        
            raise RequestError(message=f'Connect to {uri}: network problem.')
        except requests.exceptions.HTTPError:
            raise HTTPRequestError(message=f'http error has occured with {uri}.'
                               , status_code=r.status_code)
        except Exception:
            raise RequestError('Internal error has occured')
        
    def __post__(self, **kwds) -> requests.Response:
        """Perform post request"""
        return self.__request__(meth='POST', **kwds)
    
    def __get__(self, **kwds) -> requests.Response:
        """Perform get request"""
        return self.__request__(meth='GET', **kwds)
    