# -*- coding: utf-8 -*-
from exceptions.Errors import Error, TranslationNotFoundError
from model.dicts.BaseDict import BaseDict
from model.dicts.BaseDict import Receiver
from model.dicts.FullLingvoDict import FullLingvoDict
from model.dicts.ShortLingvoDict import ShortLingvoDict
from model.dicts.YandexDict import YandexDict
from model.dicts import TextAdapters
from model.dicts import Separators

# standard dictionary for multi words translations
__stand_dict__ = 'ya'

# max length of text for post request
__request_len__ = 9000


class Renderer(Receiver):
    """Receive and print translation."""
    
    def __init__(self, verb: int=0):
        self.verb = verb
    
    def set_translation(self, tran):
        self.__print__(tran, self.verb)
        
    def __print__(self, tran: BaseDict.Translation, verb: int=0):
        if verb >= 1:
            print(f'[{tran.src_lang}-{tran.dst_lang}]: {tran.source} -> {tran.tran}')
        else:
            print(tran.tran)


def rock_and_roll(text, src_lang: str, dst_lang: str, verb: int=0, dct_name: str=None, sep: str=None):
    try:
        # If certain dict is specified the one is used for translation 
        if dct_name is not None:
            dct = __get_dict__(dct_name, src_lang, dst_lang, verb)
            dct.translate(__get_adapter__(text, sep))
        # If text is just single word, translate with lingvo
        elif isWord(text):
            try:
                dct = __get_dict__('ling', src_lang, dst_lang, verb)
                dct.translate(__get_adapter__(text, sep))
            except TranslationNotFoundError:
                # Lingvo translation failed, let translate with default dict
                dct = __get_dict__(__stand_dict__, src_lang, dst_lang, verb)
                dct.translate(__get_adapter__(text, sep))
        # If text is sentence, translate with default dict 
        else:
            dct = __get_dict__(__stand_dict__, src_lang, dst_lang, verb)
            dct.translate(TextAdapters.PartIter(text))
    except Error as e:
        print(e)
        
def __get_adapter__( text: str, sep: str=None):
    """Return text adapter by type."""
    if sep == None:
        return TextAdapters.PartIter(text, __request_len__)
    elif sep == Separators.WORD:              # return adapter for words         
        return TextAdapters.Spliter(text)             
    elif sep == Separators.EXPRESSION:        # return adapter for expressions
        return TextAdapters.Spliter(text, TextAdapters.Spliter.EXPRESSION) 
    
def __get_dict__(name: str, src_lang: str, dst_lang: str, verb: int=0):
    """Return dictionary by name."""
    if name == 'ya':
        dct = YandexDict(src_lang, dst_lang, receiver=Renderer(verb))
    elif name == 'ling':
        if verb >= 2:
            dct = FullLingvoDict(src_lang, dst_lang, receiver=Renderer(verb))
        elif verb <= 1:
            dct = ShortLingvoDict(src_lang, dst_lang, receiver=Renderer(verb))
    return dct

def isWord(text: str) -> bool:
    """Return True if text is single word, False otherwise."""
    # Why it isn't a regular expression see FindWordText.py. 
    if text.__len__() == 0:
        return False
    text = text.strip()
    for char in text:
        if char.isspace():
            return False
    return True

        
        
    
    
    


