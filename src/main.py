# -*- coding: utf-8 -*-
import sys
import ArgsParser
from model import Controller


if __name__ == '__main__':                
    args = ArgsParser.parse_args(sys.argv[1:])
    Controller.rock_and_roll(
        args.TEXT, args.SOURCE_LANG, args.DEST_LANG, verb=args.verbose, dct_name=args.dict, sep=args.s)
