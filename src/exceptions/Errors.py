# -*- coding: utf-8 -*-

class Error(Exception):
    """Base class for exceptions in this module."""
    
    def __init__(self, message: str):
        self.message: str = message
        
    def __str__(self):
        return self.message


class RequestError(Error):
    """Raise when connection has failed with timeout or 
    network problems or http errors."""
    pass
        
        
class HTTPRequestError(RequestError):    
    """Raise when connection has failed with http error."""
    
    def __init__(self, status_code: int,  **kwds): 
        """Get status code and pass args to parrents"""
        self.status_code: int = status_code
        super().__init__(**kwds)
        
    def __str__(self):
        return f'{self.message} status code: [{self.status_code}]'
        
    
class ParseJsonError(Error):
    """Raise when error has appeared while parsing."""
    pass
    

class TranslationError(Error):
    """Raise when any translation error has happened."""
    pass

class TranslationNotFoundError(TranslationError):
    """Raise when translation hasn't found."""
    pass
