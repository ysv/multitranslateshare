# -*- coding: utf-8 -*-
import argparse
from model.dicts import LANGS, DICTS, Separators


prog_decript = 'Translates TEXT from SOURCE_LANG to DEST_LANG. If TEXT is one word ' \
            'it is translated through dictionaries specified under ONE_WORD_DICTS. ' \
            'Otherwise TEXT is translated through dictionaries under SENTENCE_DICTS. ' \
            'If TEXT is not defined stdin is used instead.'
           
def parse_args(args):            
    """Parse command args"""
    parser = argparse.ArgumentParser(description=prog_decript, prog='mt')
    parser.add_argument('SOURCE_LANG', choices=list(LANGS), metavar='SOURCE_LANG'
                        , help='Language to translate from')
    parser.add_argument('DEST_LANG', choices=list(LANGS), metavar='DEST_LANG'
                        , help='Language to translate to')
    parser.add_argument('TEXT', nargs='?'
                        , help='text for translation')
    parser.add_argument('-d', '--dict', choices=list(DICTS), metavar='DICT'
                       , help='specify dictionary to translate')
    parser.add_argument('-s', choices=[Separators.WORD, Separators.EXPRESSION]
                        , help='treat words or expressions at text as separate')
    parser.add_argument('-v', '--verbose', action='count', default=0
                        , help='print translation more verbosely')
    return parser.parse_args(args)
