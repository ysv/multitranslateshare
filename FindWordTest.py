# -*- coding: utf-8 -*-
import re
import time

txt = 'hello,'
def isWordCust(text: str) -> bool:
    """Return True if text is single word, False otherwise."""
    if text.__len__() == 0:
        return False
    text = text.strip()
    for char in text:
        if char.isspace():
            return False
    return True

regex = r"\s*\w+\s*"

def isWordReg(text: str):
    p = re.fullmatch(regex, text)
    if p:
        return True
    else:
        return False

start = time.perf_counter()
res = isWordCust(txt)
print(f'first func({res}):', time.perf_counter()-start)

start = time.perf_counter()
isWordReg(txt)
print(f'second func({res}):', time.perf_counter()-start)

print('-'*40)

start = time.process_time()
isWordCust(txt)
print(f'first func({res}): ', time.process_time()-start)

start = time.process_time()
isWordReg(txt)
print(f'second func({res}):', time.process_time()-start)
